/*
 * memory_map.h
 *
 *  Created on: 26 Jun 2015
 *      Author: glennan
 */

#ifndef MEMORY_MAP_H_
#define MEMORY_MAP_H_

#include <stdint.h>

// Base address for devices on the STM32F10x
//
#define FLASH_BASE_ADDR  ((uint32_t)0x08000000) // FLASH base address
#define SRAM_BASE_ADDR   ((uint32_t)0x20000000) // SRAM base address in the alias region
#define PERIPH_BASE_ADDR ((uint32_t)0x40000000) // Peripheral base address in the alias region

// Peripheral memory map
//
#define APB1_BASE_ADDR    ((uint32_t)(PERIPH_BASE_ADDR + 0x00000)) // Advanced Peripheral Bus 1
#define APB2_BASE_ADDR    ((uint32_t)(PERIPH_BASE_ADDR + 0x10000)) // Advanced Peripheral Bus 2
#define AHB1_BASE_ADDR    ((uint32_t)(PERIPH_BASE_ADDR + 0x20000)) // Advanced High-performance Bus 1

#endif /* MEMORY_MAP_H_ */
