// ----------------------------------------------------------------------------------
// gpio.c
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
// of the item whatsoever, whether express, implied, or statutory, including, but
// not limited to, any warranty of merchantability or fitness for a particular
// purpose or any warranty that the contents of the item will be error-free.
// In no respect shall Feabhas incur any liability for any damages, including, but
// limited to, direct, indirect, special, or consequential damages arising out of,
// resulting from, or any way connected to the use of the item, whether or not
// based upon warranty, contract, tort, or otherwise; whether or not injury was
// sustained by persons or property or otherwise; and whether or not loss was
// sustained from, or arose out of, the results of, the item, or any services that
// may be provided by Feabhas.
// ----------------------------------------------------------------------------------

#include "gpio.h"
#include "memory_map.h"
#include "peripherals.h"
#include <stdint.h>

// Base address for all GPIO ports
//
#define GPIO_BASE_ADDR             ((uint32_t)AHB1_BASE_ADDR)

// Simple function-like macro for specifying which
// bit in a word to set.
//
#define bit(n) (1 << (n))

// GPIO hardware registers
//
typedef struct
{
  uint32_t MODE;
  uint32_t TYPE;
  uint32_t SPEED;
  uint32_t PUSH_PULL;
  uint32_t IDR;
  uint32_t ODR;
  uint32_t BSRR;
  uint32_t LOCK;
  uint32_t ALT_FN1;
  uint32_t ALT_FN2;
} GPIO_Registers;


// Ports can be selected using an enumeration
// (Port) to index into this array.
// Port addresses can be calculated using the enum since
// all ports are at the same offset from each other.
//
static volatile GPIO_Registers* const gpio[] =
{
  (volatile GPIO_Registers*)(GPIO_BASE_ADDR + (PORT_A << 10)),
  (volatile GPIO_Registers*)(GPIO_BASE_ADDR + (PORT_B << 10)),
  (volatile GPIO_Registers*)(GPIO_BASE_ADDR + (PORT_C << 10)),
  (volatile GPIO_Registers*)(GPIO_BASE_ADDR + (PORT_D << 10)),
  (volatile GPIO_Registers*)(GPIO_BASE_ADDR + (PORT_E << 10)),
  (volatile GPIO_Registers*)(GPIO_BASE_ADDR + (PORT_F << 10))
};



void GPIO_init(Port port)
{
  AHB1_enable_device(port);
}


void GPIO_setAsOutput(Port port, Pin_number n)
{
  GPIO_configMode       (port, n, OUTPUT);
  GPIO_configOutputType (port, n, PUSH_PULL);
  GPIO_configPushPull   (port, n, NO_PUSH_PULL);
  GPIO_configOutputSpeed(port, n, LOW);
}


void GPIO_setAsInput(Port port, Pin_number n)
{
  GPIO_configMode       (port, n, INPUT);
  GPIO_configOutputType (port, n, PUSH_PULL);
  GPIO_configPushPull   (port, n, NO_PUSH_PULL);
  GPIO_configOutputSpeed(port, n, LOW);
}


void GPIO_setBit(Port port, Pin_number n)
{
  GPIO_set(port, bit(n));
}


void GPIO_set(Port port, bit_mask bits)
{
  gpio[port]->ODR |= bits;
}


void GPIO_clearBit(Port port, Pin_number n)
{
  GPIO_clear(port, bit(n));
}


void GPIO_clear(Port port, bit_mask bits)
{
  gpio[port]->ODR &= ~bits;
}


bool GPIO_isSet(Port port, Pin_number n)
{
  return ((gpio[port]->IDR & bit(n)) != 0);
}


void GPIO_configMode(Port port, Pin_number n, Mode mode)
{
  // Clear any existing configuration before
  // setting a new one.
  // Note: The mode setting is 2 bits
  //
  gpio[port]->MODE &= ~(0x03 << (n * 2));
  gpio[port]->MODE |=  (mode<< (n * 2));
}


void GPIO_configOutputType(Port port, Pin_number n, OutputType type)
{
  // Clear any existing configuration before
  // setting a new one.
  //
  gpio[port]->TYPE &= ~(0x01 << n);
  gpio[port]->TYPE |=  (type << n);
}


void GPIO_configOutputSpeed(Port port, Pin_number n, OutputSpeed speed)
{
  // Clear any existing configuration before
  // setting a new one.
  //
  gpio[port]->SPEED &= ~(0x03  << (n * 2));
  gpio[port]->SPEED |=  (speed << (n * 2));
}


void GPIO_configPushPull(Port port, Pin_number n, PushPull pupd)
{
  // Clear any existing configuration before
  // setting a new one.
  //
  gpio[port]->PUSH_PULL &= ~(0x03 << (n * 2));
  gpio[port]->PUSH_PULL |=  (pupd << (n * 2));
}


void GPIO_configAltFunction(Port port, Pin_number n, AltFunction fn)
{
  // The Alt-Fn configuration requires 4 bits per pin - hence
  // 64 bits in total.  Configuration for pins 0 - 7 is in
  // register ALT_FN1; for pins 8 - 15 use ALT_FN2
  //
  if(n < 8)
  {
    gpio[port]->ALT_FN1 &= ~(0x0F << (4 * (n % 8)));
    gpio[port]->ALT_FN1 |=  (fn   << (4 * (n % 8)));
  }
  else
  {
    gpio[port]->ALT_FN2 &= ~(0x0F << (4 * (n % 8)));
    gpio[port]->ALT_FN2 |=  (fn   << (4 * (n % 8)));
  }
}

