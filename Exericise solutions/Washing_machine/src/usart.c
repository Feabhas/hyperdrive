// ----------------------------------------------------------------------------------
// usart.c
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
// of the item whatsoever, whether express, implied, or statutory, including, but
// not limited to, any warranty of merchantability or fitness for a particular
// purpose or any warranty that the contents of the item will be error-free.
// In no respect shall Feabhas incur any liability for any damages, including, but
// limited to, direct, indirect, special, or consequential damages arising out of,
// resulting from, or any way connected to the use of the item, whether or not
// based upon warranty, contract, tort, or otherwise; whether or not injury was
// sustained by persons or property or otherwise; and whether or not loss was
// sustained from, or arose out of, the results of, the item, or any services that
// may be provided by Feabhas.
// ----------------------------------------------------------------------------------

#include "usart.h"
#include "gpio.h"
#include "stm32f4xx.h"
#include "memory_map.h"
#include "peripherals.h"
#include "msg_queue.h"
#include "usart_registers.h"


// We are using only one of the STM32F4xx's USARTs
// in this project.  In this case, USART3
// These macros allow us to port to another
// USART in the future.
//
#define USART_BASE_ADDR   (APB1_BASE_ADDR + 0x4800)
#define USART_NAME         USART_3
#define TX_GPIO            GPIO_B
#define RX_GPIO            GPIO_B
#define TX_PIN             10
#define RX_PIN             11
#define USART_IRQ          USART3_IRQn
#define USART_ISR_HANDLER  USART3_IRQHandler


// Register mappings
//
static volatile Status   * const status    = (Status*)  (USART_BASE_ADDR + 0x00);
static volatile Data     * const data      = (Data*)    (USART_BASE_ADDR + 0x04);
static volatile BaudRate * const baud_rate = (BaudRate*)(USART_BASE_ADDR + 0x08);
static volatile Control1 * const control1  = (Control1*)(USART_BASE_ADDR + 0x0C);
static volatile Control2 * const control2  = (Control2*)(USART_BASE_ADDR + 0x10);


// Helper functions
//
static inline void remap_IO_pins(void);
static inline void enable(void);
static inline void disable(void);
static inline void enable_rx_interrupt(void);
static inline void disable_rx_interrupt(void);

// Buffer for interrupt-drived operation
static QUEUE Rx_buffer;


void USART_init(USART_config * const config)
{
  // Enable the device (clock)
  //
  APB1_enable_device(USART_NAME);

  // Disable for configuration; then
  // (re)enable at the end of the function
  //
  disable();

  // Set up the data transmission
  // characteristics.
  //
  control1->PS   = config->parity;
  control1->PCE  = config->parity_en;
  control1->M    = config->data;
  control2->STOP = config->stop;

  // Baud rate must be extracted from
  // the Baud_rate enum (Strictly, this
  // is not necessary; the value could be
  // written directly to the register)
  //
  baud_rate->mantissa = config->baud >> 4;
  baud_rate->fraction = config->baud & 0x0F;

  // Configure Tx and Rx pins
  //
  remap_IO_pins();

  // Configure interrupt operation
  //
  NVIC_EnableIRQ(USART_IRQ);
  enable_rx_interrupt();
  Rx_buffer = Queue_init();

  enable();
}


void USART_send(uint8_t val)
{
  while(status->TXE == 0)
  {
    // wait...
  }
  data->Tx = val;
}


void USART_sendString(const char *str)
{
  while(*str != '\0')
  {
    USART_send(*str);
    ++str;
  }
}


uint8_t USART_get(void)
{
  bool success;
  uint8_t val;
  do
  {
    success = USART_tryGet(&val);
  } while(!success);
  return val;
}


bool USART_tryGet(uint8_t * const inout_val)
{
  bool success;
  NVIC_DisableIRQ(USART_IRQ);

  success = (Queue_get(Rx_buffer, inout_val) != QUEUE_EMPTY);

  NVIC_EnableIRQ(USART_IRQ);
  return success;
}


void USART_ISR_HANDLER(void)
{
  Queue_post(Rx_buffer, data->Rx);
}


void remap_IO_pins(void)
{
  // Each UART requires two GPIO pins to be reconfigured
  // to act as the Tx and Rx pins.  These pins are on a
  // different port, and indeed different pins, for each
  // UART.
  //
  AHB1_enable_device(TX_GPIO);
  AHB1_enable_device(RX_GPIO);

  GPIO_configAltFunction(TX_GPIO, TX_PIN, AF7);
  GPIO_configAltFunction(RX_GPIO, RX_PIN, AF7);

  GPIO_configMode(TX_GPIO, TX_PIN, ALT_FN);
  GPIO_configMode(RX_GPIO, RX_PIN, ALT_FN);

  GPIO_configOutputSpeed(TX_GPIO, TX_PIN, VERY_HIGH);
  GPIO_configOutputSpeed(RX_GPIO, RX_PIN, VERY_HIGH);

  GPIO_configPushPull(TX_GPIO, TX_PIN, PULL_UP);
  GPIO_configPushPull(RX_GPIO, RX_PIN, PULL_UP);
}


void enable(void)
{
  control1->UE = 1;
  control1->RE = 1;
  control1->TE = 1;
}


void disable(void)
{
  control1->UE = 0;
  control1->RE = 0;
  control1->TE = 0;
}


void enable_rx_interrupt(void)
{
  control1->RXEIE = 1;
}


void disable_rx_interrupt(void)
{
  control1->RXEIE = 0;
}

