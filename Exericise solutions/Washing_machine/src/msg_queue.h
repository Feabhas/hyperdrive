#ifndef MESSAGE_QUEUE_H
#define MESSAGE_QUEUE_H

#include <stdbool.h>
#include <stdint.h>

#define QUEUE_SIZE 8

typedef struct Queue* QUEUE;
typedef uint8_t Elem_Type;

typedef enum { QUEUE_OK, QUEUE_FULL, QUEUE_EMPTY } QueueError;

QUEUE      Queue_init(void);
QueueError Queue_post(QUEUE queue, Elem_Type in);
QueueError Queue_get(QUEUE queue, Elem_Type *const inout);
bool       Queue_isEmpty(QUEUE queue);

#endif // MESSAGE_QUEUE_H
