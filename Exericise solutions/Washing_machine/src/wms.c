// ----------------------------------------------------------------------------------
// wms.c
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
// of the item whatsoever, whether express, implied, or statutory, including, but
// not limited to, any warranty of merchantability or fitness for a particular
// purpose or any warranty that the contents of the item will be error-free.
// In no respect shall Feabhas incur any liability for any damages, including, but
// limited to, direct, indirect, special, or consequential damages arising out of,
// resulting from, or any way connected to the use of the item, whether or not
// based upon warranty, contract, tort, or otherwise; whether or not injury was
// sustained by persons or property or otherwise; and whether or not loss was
// sustained from, or arose out of, the results of, the item, or any services that
// may be provided by Feabhas.
// ----------------------------------------------------------------------------------


#include "gpio.h"
#include "wms.h"

typedef enum
{
  DOOR,
  PS1,
  PS2,
  PS3,
  CANCEL,
  ACCEPT,
  MOTOR_FB,
  UNUSED,
  SS_A,
  SS_B,
  SS_C,
  SS_D,
  MOTOR_CTRL,
  MOTOR_DIR,
  LATCH,
  BUZZER
} WMS_Pin;

static const Port WMS_Port = PORT_D;

void Motor_init(void)
{
  GPIO_init(WMS_Port);
  GPIO_setAsOutput(WMS_Port, MOTOR_CTRL);
  GPIO_setAsOutput(WMS_Port, MOTOR_DIR);

  Motor_off();
  Motor_direction(CW);
}


void Motor_on(void)
{
  GPIO_setBit(WMS_Port, MOTOR_CTRL);
}


void Motor_off(void)
{
  GPIO_clearBit(WMS_Port, MOTOR_CTRL);
}


void Motor_direction(MotorDirection dir)
{
  if(dir == CW) GPIO_clearBit(WMS_Port, MOTOR_DIR);
  else          GPIO_setBit(WMS_Port, MOTOR_DIR);
}


void SevenSegment_init(void)
{
  GPIO_init(WMS_Port);
  GPIO_setAsOutput(WMS_Port, SS_A);
  GPIO_setAsOutput(WMS_Port, SS_B);
  GPIO_setAsOutput(WMS_Port, SS_C);
  GPIO_setAsOutput(WMS_Port, SS_D);

  SevenSegment_blank();
}


void SevenSegment_display(unsigned int val)
{
  GPIO_clear(WMS_Port, (0x0F << SS_A));
  GPIO_set(WMS_Port, ((val & 0x0F) << SS_A));
}


void SevenSegment_blank(void)
{
  SevenSegment_display(15);
}



void ProgramKeys_init(void)
{
  GPIO_init(WMS_Port);
  GPIO_setAsOutput(WMS_Port, LATCH);
}


int  ProgramKeys_get(void)
{
  int val = 0;

  // Set the latch
  //
  GPIO_setBit(WMS_Port, LATCH);

  // While the accept key has not been pressed,
  // grab the values of the program switch
  //
  do
  {
    if(GPIO_isSet(WMS_Port, PS1)) val |= 0x01;
    if(GPIO_isSet(WMS_Port, PS2)) val |= 0x02;
    if(GPIO_isSet(WMS_Port, PS3)) val |= 0x04;

    // If Cancel is pressed, drop the latch
    // and reset.
    //
    if(GPIO_isSet(WMS_Port, CANCEL))
    {
      GPIO_clearBit(WMS_Port, LATCH);
      val = 0;
      GPIO_setBit(WMS_Port, LATCH);
    }
  } while (!GPIO_isSet(WMS_Port, ACCEPT));

  // Drop the latch and wait for the
  // Accept key has been *released*
  //
  GPIO_clearBit(WMS_Port, LATCH);
  while(GPIO_isSet(WMS_Port, ACCEPT))
  {
    // wait...
  }

  return val;
}

