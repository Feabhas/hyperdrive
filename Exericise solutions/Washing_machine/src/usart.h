// ----------------------------------------------------------------------------------
// usart.h
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
// of the item whatsoever, whether express, implied, or statutory, including, but
// not limited to, any warranty of merchantability or fitness for a particular
// purpose or any warranty that the contents of the item will be error-free.
// In no respect shall Feabhas incur any liability for any damages, including, but
// limited to, direct, indirect, special, or consequential damages arising out of,
// resulting from, or any way connected to the use of the item, whether or not
// based upon warranty, contract, tort, or otherwise; whether or not injury was
// sustained by persons or property or otherwise; and whether or not loss was
// sustained from, or arose out of, the results of, the item, or any services that
// may be provided by Feabhas.
// ----------------------------------------------------------------------------------

#ifndef USART_H_
#define USART_H_

#include <stdint.h>
#include <stdbool.h>

typedef enum
{
  BAUD_2400   = 0x1A0B,
  BAUD_9600   = 0x0683,
  BAUD_57600  = 0x0116,
  BUAD_115200 = 0x008B
} Baud_rate;


typedef enum
{
  DATA_8_BITS,
  DATA_9_BITS
} Data_length;


typedef enum
{
  STOP_1_BIT,
  STOP_05_BIT,
  STOP_2_BIT,
  STOP_15_BIT
} Stop_bits;


typedef enum
{
  PARITY_DISABLE,
  PARITY_ENABLE
} Parity_enable;


typedef enum
{
  PARITY_EVEN,
  PARITY_ODD
} Parity;


typedef struct
{
  Baud_rate     baud;
  Data_length   data;
  Stop_bits     stop;
  Parity_enable parity_en;
  Parity        parity;
} USART_config;

// -------------------------------------------
// Initialise the USART.  The USART must be
// configured by the client.  It is up to them
// to ensure the configuration is valid for
// their system.
//
// The default configuration for Feabhas
// targets is:
// baud      = BUAD_115200
// data      = DATA_8_BITS
// stop      = STOP_1_BIT
// parity_en = PARITY_DISABLE
// parity    = PARITY_ODD
//
void USART_init(USART_config * const config);

// -------------------------------------------
// Blocking send
//
void USART_send(uint8_t val);


// -------------------------------------------
// String send
//
void USART_sendString(const char *str);

// -------------------------------------------
// Blocking receive
//
uint8_t USART_get(void);

// -------------------------------------------
// Non-blocking receive.
// Will return true if a byte was received
//
bool USART_tryGet(uint8_t * const inout_val);


#endif /* USART_H_ */
