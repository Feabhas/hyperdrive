// ----------------------------------------------------------------------------------
// usart_registers.h
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
// of the item whatsoever, whether express, implied, or statutory, including, but
// not limited to, any warranty of merchantability or fitness for a particular
// purpose or any warranty that the contents of the item will be error-free.
// In no respect shall Feabhas incur any liability for any damages, including, but
// limited to, direct, indirect, special, or consequential damages arising out of,
// resulting from, or any way connected to the use of the item, whether or not
// based upon warranty, contract, tort, or otherwise; whether or not injury was
// sustained by persons or property or otherwise; and whether or not loss was
// sustained from, or arose out of, the results of, the item, or any services that
// may be provided by Feabhas.
// ----------------------------------------------------------------------------------

#ifndef USART_REGISTERS_H_
#define USART_REGISTERS_H_

// Register structures
//
// Control Register 1
//
typedef struct
{
  uint32_t SBK      : 1;
  uint32_t RWU      : 1;
  uint32_t RE       : 1;
  uint32_t TE       : 1;
  uint32_t IDLEIE   : 1;
  uint32_t RXEIE    : 1;
  uint32_t TCIE     : 1;
  uint32_t TXEIE    : 1;
  uint32_t PEIE     : 1;
  uint32_t PS       : 1;
  uint32_t PCE      : 1;
  uint32_t WAKE     : 1;
  uint32_t M        : 1;
  uint32_t UE       : 1;
  uint32_t          : 18;
} Control1;


// Control Register 2
//
typedef struct
{
  uint32_t ADD      : 4;
  uint32_t          : 1;
  uint32_t LBDL     : 1;
  uint32_t LBDIE    : 1;
  uint32_t          : 1;
  uint32_t LBCL     : 1;
  uint32_t CPHA     : 1;
  uint32_t CPOL     : 1;
  uint32_t CLKEN    : 1;
  uint32_t STOP     : 2;
  uint32_t LINEN    : 1;
  uint32_t          : 17;
} Control2;

// Divisor (for baud rate)
//
typedef struct
{
  uint32_t fraction : 4;
  uint32_t mantissa : 12;
} BaudRate;


// Status register
//
typedef struct
{
  uint32_t PE       : 1;
  uint32_t FE       : 1;
  uint32_t NE       : 1;
  uint32_t ORE      : 1;
  uint32_t RXNE     : 1;
  uint32_t TC       : 1;
  uint32_t TXE      : 1;
  uint32_t LBD      : 1;
  uint32_t CTS      : 1;
  uint32_t Reserved : 22;
} Status;

// Data register
//
typedef union
{
  uint32_t Tx;
  uint32_t Rx;
} Data;

#endif /* USART_REGISTERS_H_ */
