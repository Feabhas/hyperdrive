#include "msg_queue.h"
#include <stdint.h>
#include <assert.h>
#include <stddef.h>

#define NUMBER_QUEUES 4

struct Queue
{
  Elem_Type buffer[QUEUE_SIZE];
  uint32_t  read;
  uint32_t  write;
  uint32_t  count;
};


static QUEUE getInstance(void)
{
  static struct Queue instances[NUMBER_QUEUES];
  static uint32_t next = 0;

  if(next < NUMBER_QUEUES)
  {
    return &instances[next++];
  }
  else
  {
    return NULL;
  }
}


QUEUE Queue_init(void)
{
  QUEUE temp = getInstance();
  
  temp->count = 0;
  temp->read  = 0;
  temp->write = 0;
  
  return temp;
}

QueueError Queue_post(QUEUE queue, Elem_Type in)
{
  if(queue->count == QUEUE_SIZE) return QUEUE_FULL;
  
  queue->buffer[queue->write] = in;
  ++queue->count;
  ++queue->write;
  if(queue->write == QUEUE_SIZE) queue->write = 0;

  return QUEUE_OK;
}


QueueError Queue_get(QUEUE queue, Elem_Type *const inout)
{
  if(queue->count == 0) return QUEUE_EMPTY;

    *inout = queue->buffer[queue->read];
    --queue->count;
    ++queue->read;
    if(queue->read == QUEUE_SIZE) queue->read = 0;

    return QUEUE_OK;
}


bool Queue_isEmpty(QUEUE queue)
{
  bool empty;
  
  empty = (queue->count == 0);
  return empty;
}


