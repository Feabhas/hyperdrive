//
// This file is part of the GNU ARM Eclipse distribution.
// Copyright (c) 2014 Liviu Ionescu.
//

// ----------------------------------------------------------------------------
//#include <stdint.h>
//#include <stdio.h>

#include <stdlib.h>
#include "wms.h"
#include "usart.h"


int main(void)
{
  // Configure the WMS devices
  //
  Motor_init();
  SevenSegment_init();
  ProgramKeys_init();

#if 1
  // Configure the USART
  //
  USART_config feabhas_comms =
  {
    .baud      = BUAD_115200,
    .data      = DATA_8_BITS,
    .stop      = STOP_1_BIT,
    .parity_en = PARITY_DISABLE,
    .parity    = PARITY_EVEN
  };

  USART_init(&feabhas_comms);

  USART_sendString("\n\rHello World\n\r");

  uint8_t chr;
  do
  {
    chr = USART_get();
    USART_send(chr);

  } while(chr != 'x');

#endif



  Motor_on();
  Motor_off();
  Motor_direction(ACW);
  Motor_on();
  Motor_off();

  int val = ProgramKeys_get();
  SevenSegment_display(val);
}
