// ----------------------------------------------------------------------------------
// gpio.h
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any warranty
// of the item whatsoever, whether express, implied, or statutory, including, but
// not limited to, any warranty of merchantability or fitness for a particular
// purpose or any warranty that the contents of the item will be error-free.
// In no respect shall Feabhas incur any liability for any damages, including, but
// limited to, direct, indirect, special, or consequential damages arising out of,
// resulting from, or any way connected to the use of the item, whether or not
// based upon warranty, contract, tort, or otherwise; whether or not injury was
// sustained by persons or property or otherwise; and whether or not loss was
// sustained from, or arose out of, the results of, the item, or any services that
// may be provided by Feabhas.
// ----------------------------------------------------------------------------------

#ifndef GPIO_H_
#define GPIO_H_

#include <stdbool.h>

typedef enum
{
  PORT_A,
  PORT_B,
  PORT_C,
  PORT_D,
  PORT_E,
  PORT_F
} Port;


typedef unsigned int Pin_number;
typedef unsigned int bit_mask;

// ----------------------------------------------------
// Functional interface.
// This API gives the basic operational functions of
// the GPIO.  Use this for everyday tasks.
//
void GPIO_init(Port port);

void GPIO_setAsOutput(Port port, Pin_number n);
void GPIO_setAsInput(Port port, Pin_number n);

void GPIO_setBit(Port port, Pin_number n);
void GPIO_set(Port port, bit_mask bits);

void GPIO_clearBit(Port port, Pin_number n);
void GPIO_clear(Port port, bit_mask bits);

bool GPIO_isSet(Port port, Pin_number n);


// ----------------------------------------------------
// Configuration interface.
// This API allows fine-grained configuration of a GPIO
// port.  For normal I/O prefer the functional API.
//
typedef enum { INPUT, OUTPUT, ALT_FN, ANALOGUE }              Mode;
typedef enum { PUSH_PULL, OPEN_DRAIN }                        OutputType;
typedef enum { LOW, MEDIUM, HIGH, VERY_HIGH }                 OutputSpeed;
typedef enum { NO_PUSH_PULL, PULL_UP, PUSH_DOWN }             PushPull;
typedef enum { AF0, AF1, AF2,  AF3,  AF4,  AF5,  AF6,  AF7,
               AF8, AF9, AF10, AF11, AF12, AF13, AF14, AF15 } AltFunction;

void GPIO_configMode(Port port, Pin_number n, Mode mode);
void GPIO_configOutputType(Port port, Pin_number n, OutputType type);
void GPIO_configOutputSpeed(Port port, Pin_number n, OutputSpeed speed);
void GPIO_configPushPull(Port port, Pin_number n, PushPull pupd);
void GPIO_configAltFunction(Port port, Pin_number n, AltFunction fn);

#endif /* GPIO_H_ */
